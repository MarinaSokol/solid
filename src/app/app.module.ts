import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccreditationComponent } from './accreditation/accreditation.component';
import { BuildAccountComponent } from './build-account/build-account.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { InputComponent } from './input/input.component';
import { RiskAssesmentComponent } from './risk-assesment/risk-assesment.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { PaymentComponent } from './payment/payment.component';
import {NgxMaskModule} from 'ngx-mask'

@NgModule({
  declarations: [
    AppComponent,
    AccreditationComponent,
    BuildAccountComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    InputComponent,
    RiskAssesmentComponent,
    TermsOfUseComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgxMaskModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
