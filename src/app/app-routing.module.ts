import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccreditationComponent } from './accreditation/accreditation.component';
import {BuildAccountComponent} from './build-account/build-account.component'
import { LoginComponent } from './login/login.component';
import { RiskAssesmentComponent } from './risk-assesment/risk-assesment.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { PaymentComponent } from './payment/payment.component';
const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'accreditation', component: AccreditationComponent },
  {
    path: 'build-account',
    component: BuildAccountComponent,
  },
  { path: 'login', component: LoginComponent},
 
  { path: 'risk-asessment', component: RiskAssesmentComponent},
  { path: 'payment', component: PaymentComponent},
  {
    path: 'terms-of-use',
    component: TermsOfUseComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
