import { Component, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-accreditation',
  templateUrl: './accreditation.component.html',
  styleUrls: ['./accreditation.component.styl']
})
export class AccreditationComponent implements OnInit {
  public fileName
  public fileSize
  constructor() {
   }
  ngOnInit() {
  }
  onFileChange(event) {
    let reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    this.fileInfo(file);
  }
  allowDrop(ev) {
    ev.preventDefault();
  }
  drop(ev) {
    ev.preventDefault();
    let dt = ev.dataTransfer.files[0];
    this.fileInfo(dt);
  }
  fileInfo(file){
    this.fileName = file.name;
    this.fileSize = (file.size/(1024*1024)).toFixed(2) + 'MB';
  }

  highlight(e) {
    document.getElementById('drop-area').classList.add('highlight');
  }

  unhighlight(e) {
    document.getElementById('drop-area').classList.remove('highlight');
  }

}
