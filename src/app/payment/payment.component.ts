import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import {NgxMaskModule} from 'ngx-mask'
// import { NgxMaskModule } from 'ngx-mask'

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.styl']
})

export class PaymentComponent implements OnInit {
  myformP: FormGroup;
  cardNumber: FormControl;
  cardHolderName: FormControl;
  expDate: FormControl;
  cvv: FormControl;

  public valid = false
  constructor(private router: Router) { }

  createFormControls() {
    this.cardNumber = new FormControl(null, [
      //  Validators.required,
       Validators.pattern("[0-9]{16}"),
      ]);
    this.cardHolderName = new FormControl(null, [
        // Validators.required,
        ]);
      this.expDate = new FormControl(null, [
        // Validators.required,
        Validators.pattern("[0-9]{4}"),
       ]);
       this.cvv = new FormControl(null, [
        // Validators.required,
        Validators.pattern("[0-9]{3}"),
       ]);
  }

  createForm() {
    this.myformP = new FormGroup({
      cardNumber: this.cardNumber,
      cardHolderName: this.cardHolderName,
      expDate: this.expDate,
      cvv: this.cvv,
    });
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  onSubmit() {
    console.log(this.myformP)
    this.valid = true
    if (this.myformP.valid) {
      this.valid = false
      // this.myformP.reset();
      // this.router.navigate(['/payment']);
    }
  }

  // changeNumber(e){
  //   var text = document.getElementById("cardNumber";
  //   console.log(text);
  //   console.log('11');
  // }

}
