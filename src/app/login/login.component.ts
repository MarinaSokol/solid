import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router} from '@angular/router'; 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {
  myform: FormGroup;
  firstName: FormControl;
  email: FormControl;
  password: FormControl;
  public valid = false
  constructor (private router: Router) {

  }
  createFormControls() {
    this.firstName = new FormControl("", Validators.required);
    this.email = new FormControl(null, [
      Validators.required,
      Validators.pattern("[^ @]*@[^ @]*")
    ]);
    this.password = new FormControl("", [
      Validators.required,
      Validators.minLength(8)
    ]);
  }

  createForm() {
    this.myform = new FormGroup({
      firstName: this.firstName,
      email: this.email,
      password: this.password,
    });
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  onSubmit() {
    console.log(this.myform)
    this.valid = true
    if (this.myform.valid) {
      this.valid = false
      this.myform.reset();
      this.router.navigate(['/build-account']);
    }
  }
}
