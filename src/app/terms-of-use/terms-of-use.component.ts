import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.styl']
})
export class TermsOfUseComponent implements OnInit {
  public title = 'Terms of Use'
  public subtitle = 'Personal Information'
  public subtitle2 = 'Usage Rules'
  public content = [
    {text: 'The Platform collects certain information you elect to share when creating an account on the Platform, which may include your name, address, email address, telephone number, as well as certain information that may be included within government issued IDs, W2s, and tax returns.Collection of Personal Information by Third Parties: '},
    {text: 'Some portion of our Services may redirect you to third party websites and services that we do not operate. The privacy practices of these websites and services will be governed by their own policies. We make no representation or warranty as to the privacy policies of any third parties, including the providers of third party applications. If you are submitting information to any such third party through our Services, you should review and understand that party’s applicable policies, including their privacy policy, before providing your information to the third party.Aggregate Information:'},
    {text: 'Aggregate Information may be collected when you interact with our Services, independent of any information you voluntarily enter. Additionally, we may use one or more processes to de-identify information that contains Personal Information, such that only Aggregate Information remains. We may collect, use, store, and transfer Aggregate Information without restriction.When you use our Services some information is automatically collected. Such information could include your operating system, IP address, general statistics about your activities on the Platform’s website, the site from which you linked to us (“referring page”), the name of the website you choose to visit immediately after ours (called the “exit page”), information about other websites you have recently visited, browser (software used to browse the internet) type and language, device identifier numbers, your site activity, and the time and date of your visit. Although we do our best to honor the privacy preferences of our visitors, we are not able to respond to Do Not Track signals from your browser at this time.Cookies are alphanumeric identifiers that we transfer to your computer’s hard drive through your web browser to help us identify you when you come to our Services. You have choices with respect to cookies. By modifying your browser preferences, you have the choice to accept all cookies, to be notified when a cookie is set, or to reject all cookies. If you choose to reject all cookies you may be unable to use those aspects of our Services that require registration in order to participate. You can learn more about cookies and how they work at www.allaboutcookies.org. You can always disable cookies through your browser settings. Doing so, however, may disable certain features on our Services. You can opt-out from third party cookies that are used for advertising purposes on the NAI website at'}
  ]
  constructor() { }

  ngOnInit() {
  }

}
