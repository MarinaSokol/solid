import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-build-account',
  templateUrl: './build-account.component.html',
  styleUrls: ['./build-account.component.styl']
})
export class BuildAccountComponent implements OnInit {
  public name = 'Company Name'
  public address = 'Address Line'
  public state = 'City, State'
  public phone = 'Phone Number'
  public title = 'Your title'
  public code = 'Zip Code'
  public alt = 'Alt Phone Number'
  public country = 'Country'
  myform: FormGroup;
  zip: FormControl;
  telephone: FormControl;
  altPhone: FormControl;
  public valid = false
  constructor (private router: Router) {

  }
  createFormControls() {
    this.zip = new FormControl(null, [
      Validators.required,
      Validators.pattern("[^\d]*")
    ]);
    this.telephone = new FormControl(null, [
      Validators.required,
      Validators.pattern("[^\d]*")
    ]);
    this.altPhone = new FormControl(null, [
      Validators.required,
      Validators.pattern("[^\d]*")
    ]);
  }
  createForm() {
    this.myform = new FormGroup({
      zip: this.zip,
      telephone: this.telephone,
      altPhone: this.altPhone,
    });
  }
  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }
  onSubmit() {
    console.log(this.myform)
    this.valid = true
    // if (this.myform.valid) {
      // this.valid = false
      this.myform.reset();
      this.router.navigate(['/accreditation']);
    // }
  }
}
