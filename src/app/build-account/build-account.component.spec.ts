import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuildAccountComponent } from './build-account.component';
describe('BuildAccountComponent', () => {
  let component: BuildAccountComponent;
  let fixture: ComponentFixture<BuildAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
